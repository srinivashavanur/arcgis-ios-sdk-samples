/*
 COPYRIGHT 2017 ESRI
 
 TRADE SECRETS: ESRI PROPRIETARY AND CONFIDENTIAL
 Unpublished material - all rights reserved under the
 Copyright Laws of the United States and applicable international
 laws, treaties, and conventions.
 
 For additional information, contact:
 Environmental Systems Research Institute, Inc.
 Attn: Contracts and Legal Services Department
 380 New York Street
 Redlands, California, 92373
 USA
 
 email: contracts@esri.com
 */

#import "AGSLoadableBase.h"
#import "AGSRemoteResource.h"

@class AGSWMSServiceInfo;

/** @file AGSWMSService.h */ //Required for Globals API doc

/**  An OGC-WMS service
 
 An instance of this class represents an OGC-WMS service.
 
 @since 100.2
 */
@interface AGSWMSService : AGSLoadableBase <AGSRemoteResource>

NS_ASSUME_NONNULL_BEGIN

#pragma mark -
#pragma mark initializers

-(instancetype)init __attribute__((unavailable("init is not available.")));

/** Initialize this object with a URL to a WMS service
 @param URL to WMS service
 @return Initialized service
 @since 100.2
 */
-(instancetype)initWithURL:(NSURL *)URL;

/** Initialize this object with a URL to a WMS service
 @param URL to WMS service
 @return Initialized service
 @since 100.2
 */
+(instancetype)WMSServiceWithURL:(NSURL *)URL;

#pragma mark -
#pragma mark properties

/** Metadata of the WMS service
 @since 100.2
 */
@property (nullable, nonatomic, strong, readonly) AGSWMSServiceInfo *serviceInfo;

#pragma mark -
#pragma mark methods

NS_ASSUME_NONNULL_END

@end

