/*
 COPYRIGHT 2017 ESRI
 
 TRADE SECRETS: ESRI PROPRIETARY AND CONFIDENTIAL
 Unpublished material - all rights reserved under the
 Copyright Laws of the United States and applicable international
 laws, treaties, and conventions.
 
 For additional information, contact:
 Environmental Systems Research Institute, Inc.
 Attn: Contracts and Legal Services Department
 380 New York Street
 Redlands, California, 92373
 USA
 
 email: contracts@esri.com
 */

#import "AGSObject.h"

@class AGSENCDisplayCategories;

/** @file AGSENCMarinerSettings.h */ //Required for Globals API doc

/**  Mariner settings for ENC data
 
 Mariner settings for ENC data.

 @note Changes are applied to the display of all ENC layers in a map
 @since 100.2
 */
@interface AGSENCMarinerSettings : AGSObject

NS_ASSUME_NONNULL_BEGIN

#pragma mark -
#pragma mark initializers

-(instancetype)init __attribute__((unavailable("init is not available.")));

#pragma mark -
#pragma mark properties

/** The type of area symbolization (plain/symbolized) to use. "Symbolized" area symbolization renders area symbols using the traditional paper chart symbology. Plain reduces clutter.
 @since 100.2
 */
@property (nonatomic, assign, readwrite) AGSENCAreaSymbolizationType areaSymbolizationType;

/** The color scheme (day/dusk/night) to use
 @since 100.2
 */
@property (nonatomic, assign, readwrite) AGSENCColorScheme colorScheme;

/** Indicates whether or not to symbolize data quality (M_QUAL) for S-57 features.
 @since 100.2
 */
@property (nonatomic, assign, readwrite) BOOL dataQuality;

/** The depth of deep contour in meters.
 This must be greater than or equal to the [` safetyContour`.]
 When four (rather than two) depth shades are used, this controls the boundary between safe and deep areas.
 @since 100.2
 */
@property (nonatomic, assign, readwrite) double deepContour;

/** The ENC S-52 display categories.
 @since 100.2
 */
@property (nonatomic, strong, readonly) AGSENCDisplayCategories *displayCategories;

/** The display depth units (meters/feet/fathoms).
 @since 100.2
 */
@property (nonatomic, assign, readwrite) AGSENCDisplayDepthUnits displayDepthUnits;

/** Indicates whether or not to display national names. This is text group 31 per IHO S-52 Annex A.
 @since 100.2
 */
@property (nonatomic, assign, readwrite) BOOL displayNOBJNM;

/** Indicates whether the 'SCAMIN' (scale min) S-57 feature attribute will be used. If disabled, all ENC features will be rendered regardless of scale.
 @since 100.2
 */
@property (nonatomic, assign, readwrite) BOOL honorSCAMIN;

/** Indicates whether or not to display isolated dangers in shallow water. This is viewing group 24020, 24050 per IHO 2-52 Annex A.
 @since 100.2
 */
@property (nonatomic, assign, readwrite) BOOL isolatedDangers;

/** Indicates whether or not to display depth contour labels
 @since 100.2
 */
@property (nonatomic, assign, readwrite) BOOL labelContours;

/** Indicates whether or not to display safety contour labels
 @since 100.2
 */
@property (nonatomic, assign, readwrite) BOOL labelSafetyContours;

/** Indicates whether or not to use low accuracy symbols (CATZOC). This is viewing group 31010 per IHO S-52 Annex A.
 @since 100.2
 */
@property (nonatomic, assign, readwrite) BOOL lowAccuracy;

/** The S-52 point feature symbolization type (simplified/paperchart). Paperchart symbolization is based on traditional paper charts. Simplified symbols are more compact and visible.

 @since 100.2
 */
@property (nonatomic, assign, readwrite) AGSENCPointSymbolizationType pointSymbolizationType;

/** The depth of the safety contour in meters.
 @since 100.2
 */
@property (nonatomic, assign, readwrite) double safetyContour;

/** The depth of the shallow contour in meters.
 @since 100.2
 */
@property (nonatomic, assign, readwrite) double shallowContour;

/** Indicates whether the shallow depth pattern will be symbolized
 @since 100.2
 */
@property (nonatomic, assign, readwrite) BOOL shallowDepthPattern;

/** Indicates whether or not to display two depth shades.
 @since 100.2
 */
@property (nonatomic, assign, readwrite) BOOL twoDepthShades;

#pragma mark -
#pragma mark methods

/** Resets all ENC Mariner Settings
 @since 100.2
 */
-(void)resetToDefaults;

NS_ASSUME_NONNULL_END

@end
