//
// Copyright 2016 Esri.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

import UIKit
import ArcGIS

class ExportTilesViewController: UIViewController {
    
    @IBOutlet var mapView:AGSMapView!
    @IBOutlet var extentView:UIView!
    @IBOutlet var visualEffectView:UIVisualEffectView!
    @IBOutlet var previewMapView:AGSMapView!
    @IBOutlet var barButtonItem:UIBarButtonItem!
    
    private var graphicsOverlay = AGSGraphicsOverlay()
    private var extentGraphic:AGSGraphic!
    
    private var tiledLayer:AGSArcGISVectorTiledLayer!
    private var job:AGSExportVectorTilesJob!
    private var exportTask:AGSExportVectorTilesTask!
    var map:AGSMap!
    
    private var downloading = false {
        didSet {
            self.barButtonItem?.title = self.downloading ? "Cancel" : "Export tiles"
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //add the source code button item to the right of navigation bar
        //(self.navigationItem.rightBarButtonItem as! SourceCodeBarButtonItem).filenames = ["ExportTilesViewController"]
        
        
        self.tiledLayer = AGSArcGISVectorTiledLayer(url: URL(string: "https://basemaps.arcgis.com/arcgis/rest/services/World_Basemap_v2/VectorTileServer")!)
        
        //let map = AGSMap(basemap: AGSBasemap(baseLayer: self.tiledLayer))
        self.map = AGSMap(basemap: AGSBasemap(baseLayer: self.tiledLayer))
        //Extent for Missouri state
        //let envelope = AGSEnvelope(xMin: -95.787, yMin: 35.629, xMax: -89.072,  yMax:  40.677,  spatialReference: AGSSpatialReference.wgs84())
        //Extent for Missouri state with covering boundaries.
        let envelope = AGSEnvelope(xMin: -95.142, yMin: 35.446, xMax: -88.597,  yMax:  41.49,  spatialReference: AGSSpatialReference.wgs84())
        //Extent for Rocky Creek CA
        //let envelope = AGSEnvelope(xMin: -91.3909, yMin: 37.0654, xMax: -91.3378,  yMax:  37.1147,  spatialReference: AGSSpatialReference.wgs84())
        
        self.map.initialViewpoint = AGSViewpoint(targetExtent: envelope)
        self.mapView.map = map
     
        
        //self.mapView.setViewpoint(AGSViewpoint(targetExtent: envelope))
        

        
        //add the graphics overlay to the map view
        self.mapView.graphicsOverlays.add(self.graphicsOverlay)
        
        self.setupExtentView()
        
        self.previewMapView.layer.borderColor = UIColor.white.cgColor
        self.previewMapView.layer.borderWidth = 8
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func setupExtentView() {
        self.extentView.layer.borderColor = UIColor.red.cgColor
        self.extentView.layer.borderWidth = 2
    }
    
    func frameToExtent() -> AGSEnvelope {
        let frame = self.mapView.convert(self.extentView.frame, from: self.view)
        
        let minPoint = self.mapView.screen(toLocation: frame.origin)
        let maxPoint = self.mapView.screen(toLocation: CGPoint(x: frame.origin.x+frame.width, y: frame.origin.y+frame.height))
    
        let minMapPoint = AGSPoint(x: -88.597, y: 35.446, spatialReference: AGSSpatialReference.wgs84())
        let maxMapPoint = AGSPoint(x: -95.142, y: 41.49, spatialReference: AGSSpatialReference.wgs84())
        let extent = AGSEnvelope(min: minPoint, max: maxPoint)
        return extent
    }
    
    
    @IBAction func barButtonItemAction() {
        if downloading {
            //cancel download
            self.cancelDownload()
        }
        else {
            //download
            self.initiateDownload()
        }
    }
    
    private func cancelDownload() {
        if self.job != nil {
            //SVProgressHUD.dismiss()
            //TODO: Cancel the job when the API is available
            //self.job.cancel
            self.job = nil
            self.downloading = false
            self.visualEffectView.isHidden = true
        }
    }
    
    private func initiateDownload() {
        //set the state
        self.downloading = true
        
        //delete previous existing tpks
        self.deleteAllTpks()
        
        //initialize the export task
        self.exportTask = AGSExportVectorTilesTask(url: self.tiledLayer.url!)
        let params = AGSExportVectorTilesParameters();
        
        params.areaOfInterest=self.frameToExtent();
        params.maxLevel = 22
        
        self.exportTilesUsingParameters(params)
    }
    
    private func exportTilesUsingParameters(_ params: AGSExportVectorTilesParameters) {
        let startDate = Date()
        print("Start Time: \(startDate)")
        //destination path for the tpk, including name
        let path = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0]
        let destinationPath = "\(path)/myTileCache.vtpk"
        print("destination path: \(destinationPath)")
        
        //get the job

        self.job = self.exportTask.exportVectorTilesJob(with: params, downloadFileURL: URL(string: destinationPath)!)
        //run the job
        self.job.start(statusHandler: { (status: AGSJobStatus) -> Void in
            //show job status
            //SVProgressHUD.show(withStatus: status.statusString(), maskType: .gradient)
        }) { [weak self] (result: AnyObject?, error: Error?) -> Void in
            self?.downloading = false
            
            if let error = error {
                //SVProgressHUD.showError(withStatus: (error as NSError).localizedFailureReason, maskType: .gradient)
            }
            else {
                
                //hide progress view
                //SVProgressHUD.dismiss()
                self?.visualEffectView.isHidden = false
                
                let vectorTileCache = result as! AGSExportVectorTilesResult
                
                let newTiledLayer = AGSArcGISVectorTiledLayer(vectorTileCache:vectorTileCache.vectorTileCache!)
                self?.previewMapView.map = AGSMap(basemap: AGSBasemap(baseLayer: newTiledLayer))
                let endDate = Date()
                print("End Time: \(endDate)")
            }
        }
    }
    
    @IBAction func closeAction() {
        self.visualEffectView.isHidden = true
        
        //release the map in order to free the tiled layer
        self.previewMapView.map = nil
    }
    
    private func deleteAllTpks() {
        
        let path = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0]
        do {
            let files = try FileManager.default.contentsOfDirectory(atPath: path)
            for file in files {
                if file.hasSuffix(".vtpk") {
                    try FileManager.default.removeItem(atPath: (path as NSString).appendingPathComponent(file))
                }
            }
            print("deleted all local data")
        }
        catch {
            print(error)
        }
    }
}
